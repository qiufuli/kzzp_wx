import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

var station = () => import(/* webpackChunkName: "station" */'views/station/station') // 职位首页
var allstation= () => import(/* webpackChunkName: "profile" */'views/station/allstation') // 全部职位
var resume = () => import(/* webpackChunkName: "resume" */'views/resume/resume') // 简历首页
var profile = () => import(/* webpackChunkName: "profile" */'views/profile/profile') // 我的页面

const routes = [
  {
    path: '/station',
    name: 'station',
    meta:{
      title:'空中招聘平台'
    },
    component: station
  },
  {
    path: '/allstation',
    name: 'allstation',
    meta:{
      title:'全部职位'
    },
    component: allstation
  },
  {
    path: '/',
    redirect: '/station'
  },
  {
    path: '/resume',
    name: 'resume',
    meta:{
      title:'简历'
    },
    component: resume
  },
  {
    path: '/profile',
    name: 'profile',
    meta:{
      title:'我的'
    },
    component: profile
  },

  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
