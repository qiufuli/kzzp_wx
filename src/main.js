import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant from 'vant';
import 'vant/lib/index.css';
import 'lib-flexible';
import 'common/scss/index.scss';
import Axios from 'common/js/axios';
import loading from 'base/loading/index';
import alert from 'base/alert/index';


import Vconsole from 'vconsole'
let vConsole = new Vconsole()
Vue.use(vConsole)

Vue.use(Vant)

Vue.config.productionTip = false

Vue.prototype.$http = Axios;
Vue.prototype.$alert = alert;
// Vue.prototype.$loading = loading;
Vue.use(loading)
new Vue({
  router,
  store,
  render: h => h(App),
  mounted(){
    // 兼容ios
    document.body.addEventListener('touchstart', () => {});
  }
}).$mount('#app')

/**
 * 路由配置实现：document.title 由路由meta来决定
 * 帮助按钮设置，路由不为首页，bar显示
 */
router.afterEach(to => {
document.title = to.meta.title || '空中招聘平台';
})
