# kzzp_wx1

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## # 项目内容

## 采用vant作为前端组件库





## rem适配

### [postcss-pxtorem](https://github.com/cuth/postcss-pxtorem) vant官方推荐

是一款 postcss 插件，用于将单位转化为 rem 

```cmd
npm i postcss-pxtorem -D 这个是开发依赖
```

### [lib-flexible](https://github.com/amfe/lib-flexible) vant官方推荐

用于设置 rem 基准值

```cmd
npm i lib-flexible -S 这个是项目依赖
```

```js
// main.js
import 'lib-flexible'
```

### autoprefixer

是css兼容添加前缀

```js
 'autoprefixer': {
        browsers: ['Android >= 4.0', 'iOS >= 7']
  },
  // 新版的postcss是
  'autoprefixer': {
       overrideBrowserslist: ['Android >= 4.0', 'iOS >= 7']
  }
```

### postcss.config.js

```js
module.exports = {
  plugins: {
    'autoprefixer': {
      overrideBrowserslist: ['Android >= 4.0', 'iOS >= 7']
    },
    'postcss-pxtorem': {
      rootValue:36.0,
      propList: ['*']
    }
  }
}
```



---

