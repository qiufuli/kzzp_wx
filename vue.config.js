module.exports = {
    configureWebpack:{
        resolve:{
            alias:{
                'assets':'@/assets',
                'components':'@/components',
                'views':'@/views',
                'common':'@/common',
                'base':'@/base'
            }
        }
    },
    devServer: {
      proxy: {
          '/kzzppt_gr/*': {
              target: 'http://130.10.7.136:8086', //陈勇
              ws: true,
              changeOrigin: true, //是否跨域
          }
      },
  },
}
